/*
Copyright © 2022 Travis Jeppson <stmpy@pm.me>

*/
package main

import "container-bins/cmd"

func main() {
	cmd.Execute()
}
